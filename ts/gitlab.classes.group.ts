import * as plugins from './gitlab.plugins';
import { GitlabAccount } from './gitlab.classes.account';
import { GitlabProject } from './gitlab.classes.project';

export interface IGitlabGroup {
  id: number;
  web_url: string;
  name: string;
  path: string;
  description: string;
  visibility: string;
  share_with_group_lock: string;
  require_two_factor_authentication: string;
  two_factor_grace_period: number;
  project_creation_level: string;
  auto_devops_enabled: null;
  subgroup_creation_level: string;
  emails_disabled: null;
  mentions_disabled: null;
  lfs_enabled: boolean;
  default_branch_protection: number;
  avatar_url: string;
  request_access_enabled: boolean;
  full_name: string;
  full_path: string;
  created_at: string;
  parent_id: null;
  ldap_cn: null;
  ldap_access: null;
}

export class GitlabGroup {
  public static async getByName(nameArg: string, gitlabAccountArg: GitlabAccount) {
    const response = await gitlabAccountArg.request('GET', '/groups', {
      search: 'pushrocks',
    });
    // console.log(response);
    const returnGroup = new GitlabGroup(response[0], gitlabAccountArg);
    return returnGroup;
  }

  // INSTANCE
  public gitlabAccountRef: GitlabAccount;
  public data: IGitlabGroup;

  constructor(dataArg: IGitlabGroup, gitlabAccountArg: GitlabAccount) {
    this.gitlabAccountRef = gitlabAccountArg;
    this.data = dataArg;
  }

  public async getProjects() {
    return GitlabProject.getProjectsForGroup(this);
  }
}
